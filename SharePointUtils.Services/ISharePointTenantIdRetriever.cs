﻿using System.IO;
using System.Net;

namespace SharePointUtils.Services
{
    public interface ISharePointTenantIdRetriever
    {
        string RetrieverTenantId(string tenantUrl);
    }

    public class SharePointTenantIdRetriever : ISharePointTenantIdRetriever
    {
        public string RetrieverTenantId(string tenantUrl)
        {
            if (!tenantUrl.StartsWith("https://"))
            {
                tenantUrl = "https://" + tenantUrl;
            }

            var req = HttpWebRequest.CreateHttp(tenantUrl);

            req.Headers["Authorization"] = "Bearer";


            HttpWebResponse response;
            try
            {
                response = req.GetResponse() as HttpWebResponse;
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    return "Unable to access the web url. Please check if it is valid in your browser.";
                }

                response = webEx.Response as HttpWebResponse;
            }

            var auth = response.Headers["WWW-Authenticate"];

            var start = auth.IndexOf("realm=\"");
            start += "realm=\"".Length;

            var end = auth.IndexOf("\"", start);

            return auth.Substring(start, end-start);
        }
    }
}