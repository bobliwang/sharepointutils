﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;

namespace SharePointUtils.ModelBinder
{
    public class FromJsonBodyModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var value = actionContext.Request.Content.ReadAsStringAsync().Result;

            //var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (value == null)
            {
                return false;
            }

            var type = bindingContext.ModelType;

            var settings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            var modelValue = JsonConvert.DeserializeObject(value, type, settings);
            bindingContext.Model = modelValue;

            return true;
        }
    }
}