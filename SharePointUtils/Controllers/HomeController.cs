﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SharePointUtils.Models;

namespace SharePointUtils.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Default()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Index()
        {   
            ////string path = this.Server.MapPath("~/App_Data/ServiceDiscoveryEntries.json");
            ////return this.View(TenantUrlRepository.Instance.TenantUrls);

            return this.RedirectToAction("Default");
        }
    }
}
