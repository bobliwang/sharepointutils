﻿////using System.Linq;
////using System.Net;
////using System.Net.Http;
////using System.Threading.Tasks;
////using System.Web.Http;
////using System.Web.Http.ModelBinding;
////using Newtonsoft.Json;
////using SharePointUtils.ModelBinder;
////using SharePointUtils.Models;

////namespace SharePointUtils.Controllers
////{
////    public class ServiceDiscoveryController : ApiController
////    {
////        private ITenantUrlRepository _repo = TenantUrlRepository.Instance;

////        [HttpGet]
////        public TenantUrl GetRegionalFormsRemoteAppUrl(string tenantId)
////        {
////            if (!string.IsNullOrWhiteSpace(tenantId))
////            {
////                var result = _repo.TenantUrls.FirstOrDefault(x => x.TenantId.Equals(tenantId, System.StringComparison.InvariantCultureIgnoreCase));

////                if (result == null)
////                {
////                    return new TenantUrl { Url = "NotRegistered", Region = "NotRegistered", TenantId = tenantId };
////                }

////                return result;
////            }

////            throw new HttpResponseException(HttpStatusCode.BadRequest);
////        }

////        [HttpPost]
////        public HttpResponseMessage Register([ModelBinder(typeof(FromJsonBodyModelBinder))] TenantUrl customerInfo)
////        {
////            if (customerInfo == null)
////            {
////                return new HttpResponseMessage(HttpStatusCode.BadRequest);
////            }

////            _repo.Add(customerInfo);
////            _repo.Save();

////            return new HttpResponseMessage(HttpStatusCode.OK);
////        }

////        [HttpPost]
////        public async Task<HttpResponseMessage> Update()
////        {
////            var content = await this.Request.Content.ReadAsStringAsync();
////            _repo.TenantUrls = JsonConvert.DeserializeObject<TenantUrl[]>(content);

////            _repo.Save();

////            return new HttpResponseMessage(HttpStatusCode.OK);
////        }
////    }
////}