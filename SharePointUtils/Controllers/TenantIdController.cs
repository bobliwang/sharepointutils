﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using SharePointUtils.Attributes;
using SharePointUtils.ModelBinder;
using SharePointUtils.Models;
using SharePointUtils.Services;

namespace SharePointUtils.Controllers
{
    public class TenantIdController : ApiController
    {
        [HttpPost]
        public TenantIdsResponse RetrieveTenantIds([ModelBinder(typeof(FromJsonBodyModelBinder))] TenantIdsRequest request)
        {
            ////var json = await this.Request.Content.ReadAsStringAsync();

            ////var request = JsonConvert.DeserializeObject<TenantIdsRequest>(json);

            var tasks = new List<Task<TenantIdUrlPair>>();

            foreach (var url in request.TenantUrls)
            {
                tasks.Add(Task.Factory.StartNew<TenantIdUrlPair>(() =>
                {
                    string tenantId;

                    try
                    {
                        tenantId = new SharePointTenantIdRetriever().RetrieverTenantId(url);
                    }
                    catch (Exception ex)
                    {
                        tenantId = "Error - " + ex.Message + " => " + ex.StackTrace;
                    }
                    

                    var result = new TenantIdUrlPair
                    {
                        Url = url,
                        TenantId = tenantId
                    };

                    return result;
                }));
            }

            Task.WaitAll(tasks.ToArray());

            return new TenantIdsResponse
            {
                TenantIdUrlPairs = tasks.Select(x => x.Result).ToArray()
            };
        }
    }
    
}