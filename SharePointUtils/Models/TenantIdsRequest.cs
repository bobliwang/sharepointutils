﻿using System.Web.Http.ModelBinding;
using SharePointUtils.ModelBinder;

namespace SharePointUtils.Models
{
    public class TenantIdsRequest
    {
        public TenantIdsRequest()
        {
            this.TenantUrls = new string[0];
        }

        public string RequestId { get; set; }

        public string[] TenantUrls { get; set; }
    }

    public class TenantIdsResponse
    {
        public TenantIdsResponse()
        {
            this.TenantIdUrlPairs = new TenantIdUrlPair[0];
        }
        
        public TenantIdUrlPair[] TenantIdUrlPairs { get; set; }
    }

    public class TenantIdUrlPair
    {
        public string Url { get; set; }

        public string TenantId { get; set; }
    }
}