﻿namespace SharePointUtils.Models
{
    public class TenantUrl
    {
        public string TenantId { get; set; }

        public string Url { get; set; }

        public string Region { get; set; }
    }

    ////public class CustomerInfo
    ////{
    ////    public string TenantId { get; set; }

    ////    public string CustomerDomain { get; set; }

    ////    public string Region { get; set; }
    ////}
}