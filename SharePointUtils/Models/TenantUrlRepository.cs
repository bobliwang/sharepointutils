﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SharePointUtils.Models
{
    public interface ITenantUrlRepository
    {
        IList<TenantUrl> TenantUrls { get; set; }
        void Load();
        void Save();
        void Add(TenantUrl customerInfo);
    }
    
    public class TenantUrlRepository : ITenantUrlRepository
    {
        public static readonly TenantUrlRepository Instance;

        private string _filePath;
        
        static TenantUrlRepository()
        {
            Instance = new TenantUrlRepository
            {
                _filePath = HttpContext.Current.Request.MapPath("~/App_Data/ServiceDiscoveryEntries.json")
            };

            Instance.Load();
        }

        public TenantUrlRepository()
        {
            this.TenantUrls = new List<TenantUrl>();
        }

        public IList<TenantUrl> TenantUrls { get; set; }

        public void Load()
        {
            var json = File.ReadAllText(_filePath);
            this.TenantUrls = JsonConvert.DeserializeObject<TenantUrl[]>(json);
        }

        public void Save()
        {
            File.WriteAllText(_filePath, JsonConvert.SerializeObject(this.TenantUrls, Formatting.Indented));
        }

        public void Add(TenantUrl customerInfo)
        {
            var list = this.TenantUrls.ToList();
            list.Add(customerInfo);

            this.TenantUrls = list.ToArray();
        }
    }
}