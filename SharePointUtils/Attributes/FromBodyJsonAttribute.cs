﻿using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;

namespace SharePointUtils.Attributes
{
    public class FromBodyJsonAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override async void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var jsonText = string.Empty;

            var task = actionContext.Request.Content.ReadAsStreamAsync();
            var stream = await task;

            if (stream.CanSeek)
            {
                stream.Position = 0;
            }

            using (var sr = new StreamReader(stream))
            {
                jsonText = sr.ReadToEnd();
            }

            
            var parameters = actionContext.ActionDescriptor.GetParameters().ToList();

            foreach (var param in parameters)
            {
                if (jsonText != null)
                {   
                    var settings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
                    var paramVal = JsonConvert.DeserializeObject(jsonText, param.ParameterType, settings);

                    actionContext.ActionArguments[param.ParameterName] = paramVal;
                }
            }
        }
    }
}