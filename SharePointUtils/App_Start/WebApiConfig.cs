﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.ModelBinding.Binders;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using SharePointUtils.ModelBinder;

namespace SharePointUtils
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ////var provider = new SimpleModelBinderProvider(typeof(int[]), new FromJsonBodyModelBinder());
            ////config.Services.Insert(typeof(ModelBinderProvider), 0, provider);


            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                 name: null,
                 routeTemplate: "api/HeaderValues",
                 defaults: new { controller = "HeaderValues", action = "Headers" }
             );

            config.Routes.MapHttpRoute(
                 name: null,
                 routeTemplate: "api/ServiceDiscovery/Register",
                 defaults: new { controller = "ServiceDiscovery", action = "Register" }
             );

            config.Routes.MapHttpRoute(
               name: null,
               routeTemplate: "api/ServiceDiscovery/Update",
               defaults: new { controller = "ServiceDiscovery", action = "Update" }
            );

            config.Routes.MapHttpRoute(
                name: null,
                routeTemplate: "api/ServiceDiscovery/{tenantId}",
                defaults: new { controller = "ServiceDiscovery", action = "GetRegionalFormsRemoteAppUrl" }
            );


            config.Routes.MapHttpRoute(
                name: null,
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: null,
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
